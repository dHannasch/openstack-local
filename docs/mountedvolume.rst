==============================
Creating and mounting a volume
==============================

After attaching the volume to the instance, you should see it in ``sfdisk --list``.

.. code-block:: bash

  $ sudo sfdisk --list
  Disk /dev/sda: 40 GiB, 42949672960 bytes, 83886080 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  Disklabel type: dos

  Device     Boot   Start      End  Sectors  Size Id Type
  /dev/sda1          2048  1222655  1220608  596M 82 Linux swap / Solaris
  /dev/sda2  *    1222656 83886046 82663391 39.4G 83 Linux

  Disk /dev/sdb: 16 GiB, 17179869184 bytes, 33554432 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes


.. code-block:: bash

  $ echo 'type=83' | sudo sfdisk /dev/sdb
  Checking that no-one is using this disk right now ... OK

  Disk /dev/sdb: 16 GiB, 17179869184 bytes, 33554432 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes

  /dev/sdb1: Created a new partition 1 of type 'Linux' and of size 16 GiB.
  /dev/sdb2: Done.

  New situation:
  Disklabel type: dos

  Device     Boot Start      End  Sectors Size Id Type
  /dev/sdb1        2048 33554431 33552384  16G 83 Linux

  The partition table has been altered.
  Calling ioctl() to re-read partition table.
  Syncing disks.

  $ sudo mkfs.ext4 /dev/sdb1
  mke2fs 1.44.1 (24-Mar-2018)
  Creating filesystem with 4194048 4k blocks and 1048576 inodes

  Allocating group tables: done
  Writing inode tables: done
  Creating journal (16384 blocks): done
  Writing superblocks and filesystem accounting information: done


.. code-block:: bash

  $ sudo mkdir /mnt/mounted-volume
  $ sudo mount /dev/sdb1 /mnt/mounted-volume
  $ sudo chown 


http://www.darwinbiler.com/openstack-creating-and-attaching-a-volume-into-an-instance/
