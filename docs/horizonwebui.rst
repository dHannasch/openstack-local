========================
OpenStack Horizon Web UI
========================

Apart from https://pypi.org/project/python-openstackclient/, you can use the OpenStack web UI (named Horizon, although that name does not appear in the UI itself).

First, when logging in, you will need to set the domain.

.. raw:: html
  :file: login_domain.html

The first thing to do is not to create an instance, but to create a network. Go to Network->Networks in the sidebar.

.. raw:: html
  :file: sidebar.html

And click the Create Network button.

.. raw:: html

  <style>
  .btn-default {
    color: #333;
    background-color: #fff;
    border-color: #ccc;
  }
  </style>
  <a id="networks__action_create" class="btn data-table-action btn-default ajax-modal" href="/project/networks/create" title="Create Network"><span class="fa fa-plus"></span> Create Network</a>

The network and subnet can be named anything. The subnet Network Address can be 192.168.0.0/24 as suggested by the tooltip. The Gateway IP can be left blank to fill in the default.

.. raw:: html
  :file: subnet.html

Fill in the subnet DNS Name Servers with the local IPs as shown.

.. raw:: html
  :file: subnet_dns_name_servers.html

Once the network is set up, it needs a router. You should see Routers right under Networks in the sidebar.

After creating a Router (you cannot do this while creating the Router), click on the Router you just created, click to the Interfaces tab, and click the Add Interface button. For Subnet, select the only Subnet you created. You can leave IP Address blank.

Network Topology should show the Router linking the External Network to the Network you created.

.. since the raw HTML above already defined the btn-default class, we need not do so again

.. raw:: html

  <a id="routers__action_create" class="btn data-table-action btn-default ajax-modal" href="/project/routers/create/" title="Create Router"><span class="fa fa-plus"></span> Create Router</a>

Under Routers should be Security Groups. You should already have at least one security group, called ``default``. You just need to click the Manage Rules button. (Optionally, you can create a separate Security Group to allow SSH access and leave the default Security Group denying it, if you have a use for VMs to which you do not have SSH access. If you do that, you will need to select the non-default Security Group each time you Launch Instance.)

.. raw:: html

  <a class="btn data-table-action btn-default "> Manage Rules</a>

In the Rules for the default Security Group, you will likely see that Egress is allowed on any port, but Ingress is not allowed at all. To control a VM via SSH, you will need to allow Ingress on port 22. To do that, you need to Add Rule.

.. raw:: html

  <a id="rules__action_add_rule" class="btn data-table-action btn-default ajax-modal" title="Add Rule"><span class="fa fa-plus"></span> Add Rule</a>

Fortunately, you can just select SSH from the dropdown and it will populate the Rule with sane values. The default CIDR 0.0.0.0/0 will allow SSH access from any IP address.

.. raw:: html
  :file: AddRule.html

Below Security Groups, you should see Floating IPs.

In theory these IP addresses can "float", but in normal use, for the sake of your own sanity, it's usually best to have exactly one Floating IP per instance and give the Floating IP and the instance the same or related names.

Floating IP in hand, we are finally ready to go back to Compute->Instances and the Launch Instance button.

.. raw:: html

  <a id="instances__action_launch-ng" ng-click="modal.openLaunchInstanceWizard({ successUrl: '/project/instances/' })" ng-controller="LaunchInstanceModalController as modal" class="btn data-table-action btn-default btn-launch ng-scope" href="javascript:void(0);" title="Launch Instance"><span class="fa fa-cloud-upload"></span> Launch Instance</a>

Instance Name can be anything you like. For the Boot Source, you will usually want a fresh Image. Flavor will naturally depend on your needs.

With your Networks and Security Groups already set up as default, you can skip those when you Launch Instance. The only thing you need to be sure to select is the Key Pairs you will use to SSH in (otherwise there would be no point in allowed SSH ingress).





The compute service is called Nova.
https://opendev.org/openstack/nova
https://docs.openstack.org/nova/latest/

Unfortunately, https://docs.openstack.org/horizon/latest/user/launch-instances.html does not tell you most of what you will actually need to know.

https://opendev.org/openstack/python-novaclient
https://docs.openstack.org/python-novaclient/latest/reference/index.html
https://docs.openstack.org/python-novaclient/latest/user/python-api.html
https://docs.openstack.org/zh_CN/user-guide/common/cli-set-environment-variables-using-openstack-rc.html
https://docs.openstack.org/nova/wallaby/user/launch-instances.html

