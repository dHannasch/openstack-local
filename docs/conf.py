# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import os
import traceback

import jinja2

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.coverage',
    'sphinx.ext.doctest',
    'sphinx.ext.extlinks',
    'sphinx.ext.ifconfig',
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
]
source_suffix = '.rst'
master_doc = 'index'
project = 'OpenStack Local'
year = '2021'
author = 'David Hannasch'
copyright = '{0}, {1}'.format(year, author)
try:
    from pkg_resources import get_distribution
    version = release = get_distribution('openstack_local').version
except Exception:
    traceback.print_exc()
    version = release = '0.0.0'

pygments_style = 'trac'
templates_path = ['.']
extlinks = {
    'issue': ('https://gitlab.com/dHannasch/openstack-local/issues/%s', '#'),
    'pr': ('https://gitlab.com/dHannasch/openstack-local/pull/%s', 'PR #'),
}
# on_rtd is whether we are on readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

if not on_rtd:  # only set the theme if we're building docs locally
    html_theme = 'sphinx_rtd_theme'

html_static_path = ['_static']
html_use_smartypants = True
html_last_updated_fmt = '%b %d, %Y'
html_split_index = False
html_sidebars = {
    '**': ['searchbox.html', 'globaltoc.html', 'sourcelink.html'],
}
html_short_title = '%s-%s' % (project, version)

napoleon_use_ivar = True
napoleon_use_rtype = False
napoleon_use_param = False

DNS_IPs = json.loads(os.environ['DNS_IPs_JSON_list'])
with open('subnet_dns_name_servers.html') as subnet_dns_name_servers_file:
  template = jinja2.Template(subnet_dns_name_servers_file.read())
with open('subnet_dns_name_servers.html', 'w') as subnet_dns_name_servers_file:
  subnet_dns_name_servers_file.write(template.render(DNS_NAME_SERVERS_ONE_PER_LINE='\n'.join(DNS_IPs)))
