openstack_local
===============

.. testsetup::

    from openstack_local import *

.. automodule:: openstack_local
    :members:
